﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{
    public static GameController instance;
    public GameObject MainPanel;
    public Text Score;
    private static int score = 1000;

    // Start is called before the first frame update
    void Start()
    {
        MainPanel.SetActive(false);
        Score.text = "" + score;
    }
    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
        }else if(instance != this)
        {
            Destroy(gameObject);
        }
    }

    // Update is called once per frame
    void Update()
    { 
        if (Input.GetKeyDown("escape"))
        {
            MainPanel.SetActive(true);
        }
        if (Input.GetKeyDown("3"))
        {
            Score.text = "" + (score += 10);
        }
    }


}
