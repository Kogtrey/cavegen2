﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player_Movement : MonoBehaviour
{
    public Rigidbody2D player;
    public Animator animation;
    public int speed;
    private Vector2 currV;
    // Start is called before the first frame update
    void Start()
    {
        player = GetComponent<Rigidbody2D>();
        animation.GetComponent<Animator>();
        currV = player.velocity;
    }

    // Update is called once per frame
    void Update()
    {
        Vector2 newV = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));
        currV = newV * speed;
        if (Input.GetKey("left shift"))
        {
            currV *= 2;
        }
        player.velocity = currV;

        animation.SetBool("wPressed", false);
        animation.SetBool("aPressed", false);
        animation.SetBool("sPressed", false);
        animation.SetBool("dPressed", false);

        if (Input.GetKey("left shift"))
        {
            player.velocity = currV * 2;
        }
        if (Input.GetKey("w"))
        {
            animation.SetBool("wPressed", true);
        }
        if (Input.GetKey("a"))
        {
            animation.SetBool("aPressed", true);
        }
        if (Input.GetKey("s"))
        {
            animation.SetBool("sPressed", true);
        }
        if (Input.GetKey("d"))
        {
            animation.SetBool("dPressed", true);
        }
    }
}
