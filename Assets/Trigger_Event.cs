﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Trigger_Event : MonoBehaviour 
{
    private BoxCollider2D collider;
    private bool Triggered = false;
    private bool Collision;
    // Start is called before the first frame update
    void Start()
    {
        collider = GetComponent<BoxCollider2D>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag == "Player")
        {
            Triggered = true;
            LoadMap(this.tag);
        }
    }
    public void LoadMap(string MapName)
    {
        SceneManager.LoadScene(MapName);
    }
    public bool isTriggered
    {
        get
        {
            return Triggered;
        }
        set
        {
            Triggered = false;
        }
    }
}
